#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/**********************************************
* USE THIS OPTION TO USE OPTIMIZED MATRICES
**********************************************/
#define OPTIMIZED_MATRIX


/**********************************************
* USE THIS OPTION FOR A MULTI-THREADED VERSION
**********************************************/
//#define MULTI_THREADED



#define TRUE 1
#define FALSE 0

const unsigned int N = 800;
const unsigned int MAX_THREADS = 10;
void Task(unsigned int);


#ifdef OPTIMIZED_MATRIX
	void MatrixMultiplication(unsigned int, double *, double *, double *);
	double * MatrixInitialization(unsigned int, int, unsigned int);
	void DestroyMatrix(unsigned int, double *);
#else
	void MatrixMultiplication(unsigned int, double **, double **, double **);
	double ** MatrixInitialization(unsigned int, int, unsigned int);
	void DestroyMatrix(unsigned int, double **);
#endif

#ifdef MULTI_THREADED
#include <pthread.h>

	void * ThreadJob(void * dummy)
	{
		Task(N);
		return NULL;
	}
#endif



/**********************************************
* MAIN
**********************************************/
int main(int argc, char* argv[])
{
#ifdef MULTI_THREADED
	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s <thread_count>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	unsigned int nThreads = atoi(argv[1]);
	if ((nThreads <= 0) || (nThreads > MAX_THREADS))
	{
		fprintf(stderr, "ERROR: thread count should be between [1,10]\n");
		exit(EXIT_FAILURE);
	}

	printf("Running with %d thread(s)...\n", nThreads);
	pthread_t Threads[nThreads];

	// Start threads
	for (int i = 0; i < nThreads; i++)
	{
		int pthread_ret = pthread_create(&Threads[i], NULL, ThreadJob, NULL);
		if (pthread_ret)
		{
			fprintf(stderr, "ERROR: pthread_create error code: %d.\n", pthread_ret);
			exit(EXIT_FAILURE);
		}
	}

	// Wait for the threads to finish
	for (int i = 0; i < nThreads; i++)
	{
		pthread_join(Threads[i], NULL);
	}
#else
	// The single thread of the process runs the task
	Task(N);
#endif
	return 0;
}



/**********************************************
* TASK
**********************************************/
void Task(unsigned int nElements)
{
#ifdef OPTIMIZED_MATRIX
	double 	* matrixA,
		* matrixB,
		* matrixResult;
#else
	double	** matrixA,
		** matrixB,
		** matrixResult;
#endif

	// Initialize matrices
	matrixA = MatrixInitialization(nElements, TRUE, time(NULL));
	matrixB = MatrixInitialization(nElements, TRUE, time(NULL));
	matrixResult = MatrixInitialization(nElements, FALSE, 0);
	if (!matrixA || !matrixB || !matrixResult)
	{
		fprintf(stderr, "ERROR in Task: Cannot allocate memory\n");
		exit(EXIT_FAILURE);
	}

	// Multiplication
	MatrixMultiplication(nElements, matrixA, matrixB, matrixResult);

	// Destroy matrices
	DestroyMatrix(nElements, matrixA);
	DestroyMatrix(nElements, matrixB);
 	DestroyMatrix(nElements, matrixResult);
}


double getRandom(unsigned int min, unsigned int max)
{
	// Pseudo-random numbers in the interval [min,max]
	return min + (max - min) * ((double)rand()/(double)RAND_MAX);
}


#ifdef OPTIMIZED_MATRIX
double * MatrixInitialization(unsigned int nElements, int random, unsigned int seed)
{
	double * matrix = (double *) malloc(nElements * nElements * sizeof(double));
	if (!matrix)
		return NULL;

	if (random)
	{
		for (int i = 0; i < nElements; i++)
		{
			for (int j = 0; j < nElements; j++)
			{
				// Pseudo-random numbers in the interval [1.0-2.0]				
				matrix[i * nElements + j] = getRandom(1,2);
			}
		}
	}

	return matrix;
}


void MatrixMultiplication(unsigned int nElements, double * A, double * B, double * R)
{
	for (int i = 0; i < nElements; i++)
	{
		for (int j = 0; j < nElements; j++)
		{
			int index = i * nElements + j;
			R[index] = 0;
			for (int k = 0; k < nElements; k++)
			{
				R[index] += A[i * nElements + k] * B[k * nElements + j];
			}
		}
	}
}


void DestroyMatrix(unsigned int nElements, double * M)
{
    free(M);
}


#else


double ** MatrixInitialization(unsigned int nElements, int random, unsigned int seed)
{
	double ** matrix = (double **) malloc(nElements * sizeof(double *));
	if (!matrix)
		return NULL;

	for (int i = 0; i < nElements; i++)
	{
		matrix[i] = (double *) malloc(nElements * sizeof(double));
		if (!matrix)
			return NULL;

		if (random)
		{
			for (int j = 0; j < nElements; j++)
			{
				// Pseudo-random numbers in the interval [1.0-2.0]
				matrix[i][j] = getRandom(1,2);
			}
		}
	}

	return matrix;
}


void MatrixMultiplication(unsigned int nElements, double ** A, double ** B, double ** R)
{
	for (int i = 0; i < nElements; i++)
	{
		for (int j = 0; j < nElements; j++)
		{
			R[i][j] = 0;
			for (int k = 0; k < nElements; k++)
			{
				R[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}


void DestroyMatrix(unsigned int nElements, double ** M)
{
	for (int i = 0; i < nElements; i++)
	{
		free(M[i]);
	}
    free(M);
}
#endif
