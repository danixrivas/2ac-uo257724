;
;Cuenta los elementops de un string
;

	.data
string:  .asciiz "Hola mundo"

	.text
	
main:
	xor r5,r5,r5 ;r5=0
	xor r6,r6,r6 ;Indice de elemento a 0
	
loop:
	lbu r4,string(r5) 
	beqz r4,end ;Final del string?
	daddui r5, r5, 1 ;Sumamos el nuevo elemento
	j loop
	
end:
	sd r5, result(r0)
	halt ;Final ejecucion
	
	