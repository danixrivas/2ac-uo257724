#include <stdio.h>

int main() {

	char *p = NULL;
	int *q = NULL;
	int a = 3;
   
	printf("Program starts\n");

	__asm__ (
		"mov %ecx, %eax"
	);

     a = (int) *(&q + 0x33333333); 
     p = (char *)&a +  0x100000;
     printf("p-> %p\n", p);

	__asm__ (
		"sti"
	);
   

	printf("Program ends\n");
	
	return 0;
}

